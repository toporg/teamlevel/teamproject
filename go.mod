module teamproject

go 1.13

require gitlab.com/toporg/libraries/testlib v0.0.1

replace gitlab.com/toporg/libraries/testlib => gitlab.com/toporg/libraries/testlib.git v0.0.1
